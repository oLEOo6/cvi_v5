﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace CVI_v5.Converter
{
    class EEPROM_FlagConverter : IValueConverter
    {
        // For ConvertBack
        byte tmp;

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            tmp = (byte)value;
            var shift = int.Parse((string)parameter);
            var result = (tmp & (1 << shift)) > 0;
            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var bo = (bool)value;
            var shift = int.Parse((string)parameter);
            if (bo)
                tmp = (byte)(tmp | (1 << shift));
            else
                tmp = (byte)(tmp & ~(1 << shift));

            return tmp;
        }
    }
}
