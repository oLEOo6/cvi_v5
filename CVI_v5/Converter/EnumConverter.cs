﻿using CVI_v5.Common;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace CVI_v5.Converter
{
    class EnumConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var Etype = (string)parameter;
            var val = (int)(double)value;
            var rt = string.Empty;

            switch (Etype)
            {
                case "Status":
                    if (val == 0)
                        return "GOOD";

                    foreach(var e in Enum.GetValues(typeof(Status)))
                    {
                        if ((val & (int)e) == (int)e)
                            rt += e.ToString() + " ";
                    }
                    break;
                case "Relay":
                    if (val == 0)
                        return string.Empty;

                    foreach (var e in Enum.GetValues(typeof(Relay)))
                    {
                        if ((val & (int)e) == (int)e)
                            rt += e.ToString() + " ";
                    }
                    break;
            }

            return rt;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
