﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;

namespace CVI_v5.Converter
{
    class ComboBoxItemConverter : IMultiValueConverter
    {
        //public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        //{
        //    return new ComboBoxItem() { Content = (int)value };
        //}

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var val = (int)values[0];
            var col = (ComboBox)values[1];
            Console.WriteLine("AAA:" + col.Items.Count);
            foreach(var v in col.Items)
            {
                Console.WriteLine("QQQ");
                if ((int)((ComboBoxItem)v).Content == val)
                    return v;
            }
            Console.WriteLine("NOOOOOOOOOOOOOOOO");
            return null;
        }

        //public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        //{
        //    return (int)((ComboBoxItem)value).Content;
        //}

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return new object[] { (int)((ComboBoxItem)value).Content };
        }
    }
}
