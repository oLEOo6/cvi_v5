﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CVI_v5
{
    /// <summary>
    /// MainWindow.xaml 的互動邏輯
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            //Task.Run(() =>
            //{
            //    while (true)
            //        System.Threading.Thread.Sleep(1);
            //});
            //Task.Run(() =>
            //{
            //    while (true)
            //        System.Threading.Thread.Sleep(1);
            //});
            
        }

        private void dragME(object sender, MouseButtonEventArgs e)
        {
            try
            {
                DragMove();
            }
            catch (Exception)
            {
                //throw;
            }
        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void ToggleButton_Click(object sender, RoutedEventArgs e)
        {
            if (WindowSZ.IsChecked == true)
                WindowState = WindowState.Maximized;    //使視窗最大化
            else
                WindowState = WindowState.Normal;
            //WindowState = WindowState.Minimized;    //使視窗最小化
        }
    }
}
