﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CVI_v5.View
{
    /// <summary>
    /// DetailView.xaml 的互動邏輯
    /// </summary>
    public partial class DetailView : UserControl
    {
        public DetailView()
        {
            InitializeComponent();
        }

        public bool Expaned { get; set; } = false;
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            foreach (var rectangle in FindVisualChildren<Expander>(this))
            {
                if (rectangle.Name == "VolCelExp" || rectangle.Name == "TemCelExp")
                {
                    (rectangle).IsExpanded = Expaned;
                }
            }
        }

        // 找子元件函式
        public IEnumerable<T> FindVisualChildren<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj != null)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(depObj, i);

                    if (child != null && child is T)
                        yield return (T)child;

                    foreach (T childOfChild in FindVisualChildren<T>(child))
                        yield return childOfChild;
                }
            }
        }
    }
}
