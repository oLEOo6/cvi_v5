﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CVI_v5.View
{
    /// <summary>
    /// SettingView.xaml 的互動邏輯
    /// </summary>
    public partial class SettingView : UserControl
    {
        public ObservableCollection<int> CAN_Combo { get; set; } = new ObservableCollection<int>() { 250, 500, 1000 };
        public ObservableCollection<int> MODBUS_SPEED_Combo { get; set; } = new ObservableCollection<int>() { 9600, 19200, 38400, 115200 };
        public ObservableCollection<string> MODBUS_PORT_Combo { get; set; } = new ObservableCollection<string>();

        public ObservableCollection<int> PackNumber_Combo { get; set; } = new ObservableCollection<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
        public ObservableCollection<int> VoltageCell_Combo { get; set; } = new ObservableCollection<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
        public ObservableCollection<int> TemperatureCell_Combo { get; set; } = new ObservableCollection<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };

        public SettingView()
        {
            InitializeComponent();
            
            foreach (var s in System.IO.Ports.SerialPort.GetPortNames())
                MODBUS_PORT_Combo.Add(s);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
