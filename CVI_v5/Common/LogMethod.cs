﻿using CVI_v5.Model;
using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace CVI_v5.Common
{
    public class LogMethod : ViewModelBase
    {
        public string Name { get; set; }
        public string Path { get; set; }
        private string FullPath = string.Empty;
        public double Interval
        {
            get
            {
                return Timer.Interval;
            }
            set
            {
                Timer.Interval = value;
            }
        }

        public Timer Timer = new Timer();
        public bool Running { get; set; } = false;
        private int Count = 0;
        public int MaxCount { get; set; } = 50000;
        private string BeforeDay = DateTime.Now.ToString("dd");

        // Log 集合
        public List<DataName> DataSet = new List<DataName>(); 

        public LogMethod()
        {
            Timer.Elapsed += Log;
        }

        public void LogStop()
        {
            Running = false;
            Timer.Stop();
        }

        // LogInterval Unit: ms
        public void FirstTime(string FileName, string FilePath, double LogInterval)
        {
            // 初始化
            this.Name = FileName;
            this.Path = FilePath;
            this.Interval = LogInterval;
            BeforeDay = DateTime.Now.ToString("dd");

            //// 彈出選擇路徑視窗
            //Path = PathSelect(Path);

            // 建立檔案
            FileCreate();

            // 開始記錄Loop
            Timer.Start();
            Running = true;
        }

        private void FullPathUpdate()
        {
            var Num = 1;
            do
            {
                FullPath = Path + @"\" + Name + "-" + DateTime.Now.ToString("yyyy-MM-dd") + "_" + Num++ + ".csv";
            } while (File.Exists(FullPath));
        }
        public void FileCreate()
        {
            // 檢測路徑是否存在，若否則建立目錄
            if (!Directory.Exists(Path))
                Directory.CreateDirectory(Path);

            FullPathUpdate();

            var sw = new StreamWriter(FullPath, true);

            var SB = new StringBuilder();
            SB.Append("Time,"); // DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss")

            foreach (DataName data in DataSet)
                SB.Append(data.Name + ',');

            sw.WriteLine(SB.ToString());
            sw.Close();
        }

        private void Log(object sender, ElapsedEventArgs e)
        {
            // 跨日判斷
            if (BeforeDay != DateTime.Now.ToString("dd"))
                FileCreate();

            BeforeDay = DateTime.Now.ToString("dd");

            StreamWriter sw = new StreamWriter(FullPath, true);

            var SB = new StringBuilder();
            SB.Append(DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ',');

            foreach (DataName data in DataSet)
                SB.Append(data.Value + ",");

            sw.WriteLine(SB.ToString());
            sw.Close();

            Count++;

            if (Count >= MaxCount)
            {
                Count = 0;
                FileCreate();
            }

            sw.Close();
        }

        // 需要加入參考 System.Windows.Forms
        public string PathSelect(string path)
        {
            var openBrowserDialog = new System.Windows.Forms.FolderBrowserDialog
            {
                SelectedPath = path
            };
            var result = openBrowserDialog.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.Cancel)
                throw new Exception("Cancel");

            return openBrowserDialog.SelectedPath;
        }
        // 需要加入參考 System.Windows.Forms
        public string FileSelect()
        {
            var openFileDialog = new Microsoft.Win32.OpenFileDialog();
            var result = openFileDialog.ShowDialog();
            if (result == true)
            {
                var s = openFileDialog.FileName;
                Console.WriteLine(s);
            }
            return openFileDialog.FileName;
        }
    }
}
