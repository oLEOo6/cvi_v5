﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CVI_v5.Common
{
    public class ErrorOutput
    {
        public static void ErrorLog(string ErrorString)
        {
            try
            {
                StreamWriter sw = new StreamWriter(Environment.CurrentDirectory + @"\Error.txt", true);

                var SB = new StringBuilder();
                SB.Append(DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ':');
                SB.Append(ErrorString);

                PrintTrace(SB.ToString());

                sw.WriteLine(SB.ToString());
                sw.Close();
            }
            catch (Exception) {
                PrintTrace("Open same File!");
            }
        }

        public static void PrintTrace(string content)
        {
            Trace.WriteLine(content);
        }
    }
}
