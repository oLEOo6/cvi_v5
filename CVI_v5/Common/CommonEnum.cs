﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CVI_v5.Common
{
    public enum CommunicationInterface
    {
        CANBUS,
        MODBUS,
        ETHERNET,
        BLUETOOTH
    }

    public enum Status
    {
        //OK = 0x0000,
        OV = 0x0001,
        UV = 0x0002,
        OT_C = 0x0004,
        OT_D = 0x0008,
        UT_C = 0x0010,
        UT_D = 0x0020,
        OC_C = 0x0040,
        OC_D = 0x0080,
        ERROR = 0x0100,
        SC = 0x0200,
        IR_Lv1 = 0x0400,
        IR_Lv2 = 0x0800,
        CP_Sticking = 0x1000,
        CN_Sticking = 0x2000,
        DP_Sticking = 0x4000,
        DN_Sticking = 0x8000
    }

    public enum Relay
    {
        CHG_P = 0x01,
        CHG_N = 0x02,
        DSG_P = 0x04,
        DSG_N = 0x08,
        OBC = 0x10,
        preCHG = 0x20,
    }

    class CommonEnum
    {
    }
}
