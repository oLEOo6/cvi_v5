﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CVI_v5.Common
{
    class ModBus
    {
        //ModBus CRC函式
        public static byte[] ModRTU_CRC(byte[] buf, int len)
        {
            UInt16 crc = 0xFFFF;

            for (int pos = 0; pos < len; pos++)
            {
                crc ^= (UInt16)buf[pos];          // XOR byte into least sig. byte of crc

                for (int i = 8; i != 0; i--)
                {    // Loop over each bit
                    if ((crc & 0x0001) != 0)
                    {      // If the LSB is set
                        crc >>= 1;                    // Shift right and XOR 0xA001
                        crc ^= 0xA001;
                    }
                    else                            // Else LSB is not set
                        crc >>= 1;                    // Just shift right
                }
            }
            // Note, this number has low and high bytes swapped, so use it accordingly (or swap bytes)
            //return crc;
            return new byte[] { (byte)crc, (byte)(crc >> 8) };
        }
        //陣列加兩位CRC
        public static void AppendCRC(byte[] input)
        {
            byte[] crc = ModRTU_CRC(input, input.Length);
            Array.Resize(ref input, input.Length + 2);
            input[input.Length - 2] = crc[0];
            input[input.Length - 1] = crc[1];
        }
        //ModBus CRC 檢查 true:正確 false:錯誤
        public static bool CheckCRC(byte[] input)
        {
            if (input.Length < 3)
                return false;
            byte[] crc = ModRTU_CRC(input, input.Length - 2);
            if (input[input.Length - 2] == crc[0] && input[input.Length - 1] == crc[1])
                return true;
            return false;
        }

        public static bool AckConfirm(byte[] data, int dataLength, byte ID, byte functionCode)
        {
            if (CheckCRC(data) && dataLength == data.Length && ID == data[0] && functionCode == data[1])
                return true;
            return false;
        }
    }
}
