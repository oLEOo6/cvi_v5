﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CVI_v5.Model
{
    public class DataName : ViewModelBase
    {
        public double Value { get; set; }
        public string Name { get; set; }

        public DataName(string Name)
        {
            this.Name = Name;
        }
    }
}
