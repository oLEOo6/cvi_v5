﻿using CVI_Lib;
using System;
namespace CVI_v5.Model
{
    [Serializable]
    public abstract class DataBasic: ViewModelBase
    {
        public virtual int Index { get; set; }
        public virtual int Length { get; set; }
        public virtual bool BigEndian { get; set; }
        public virtual bool Sign { get; set; }
        public byte[] data;
        public virtual byte[] Data
        {
            get
            {
                return data;
            }
            set
            {
                data = value;
                OnPropertyChanged("Value");
            }
        }
        public double Value
        {
            get
            {
                return GetValue();
            }
            set
            {
                SetValue(value);
                OnPropertyChanged("Value");
            }
        }
        public virtual EEtype EEtype { get; set; }
        public virtual string Name { get; set; }
        public virtual string Unit { get; set; }

        public abstract double GetValue();
        public abstract void SetValue(double Input);
    }
}
