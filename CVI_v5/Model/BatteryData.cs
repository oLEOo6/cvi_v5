﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CVI_v5.Model
{
    [Serializable]
    public class BatteryData : ViewModelBase
    {
        // 主要資料
        public DataName SOC { get; set; } = new DataName("SOC");
        public DataName Voltage { get; set; } = new DataName("Voltage");
        public DataName Current { get; set; } = new DataName("Current");
        public DataName Temperature { get; set; } = new DataName("Temperature");
        public DataName Status { get; set; } = new DataName("Status");
        public DataName Relay { get; set; } = new DataName("Relay");

        // 敬閔新增
        public DataName VoltageMax { get; set; } = new DataName("VoltageMax");
        public DataName VoltageMin { get; set; } = new DataName("VoltageMin");
        public DataName TemperatureMax { get; set; } = new DataName("TemperatureMax");
        public DataName TemperatureMin { get; set; } = new DataName("TemperatureMin");
        public DataName CurrentCHG { get; set; } = new DataName("CurrentCHG");
        public DataName CurrentDSG { get; set; } = new DataName("CurrentDSG");
        public DataName ErrorCode { get; set; } = new DataName("ErrorCode");
        // Need?
        public DataName Cycle { get; set; } = new DataName("Cycle");
        public DataName ChargeTime { get; set; } = new DataName("ChargeTime");
        public DataName UseTime { get; set; } = new DataName("UseTime");
        public DataName Year { get; set; } = new DataName("Year");
        public DataName Week { get; set; } = new DataName("Week");
        public DataName FirmwareVersion { get; set; } = new DataName("FirmwareVersion");
        // 並聯資料
        public ParallelData ParallelData { get; set; } = new ParallelData();

        // Pack資料
        public DataName PackVoltageMax { get; set; } = new DataName("PackVoltageMax");
        public DataName PackVoltageMin { get; set; } = new DataName("PackVoltageMin");
        public DataName PackVoltageRange { get; set; } = new DataName("PackVoltageRange");
        public bool PackVoltageRangeFlag { get; set; } = false;
        public DataName PackTemperatureMax { get; set; } = new DataName("PackTemperatureMax");
        public DataName PackTemperatureMin { get; set; } = new DataName("PackTemperatureMin");
        public DataName PackTemperatureRange { get; set; } = new DataName("PackTemperatureRange");
        public bool PackTemperatureRangeFlag { get; set; } = false;

        public ObservableCollection<PackVoltage> PackVoltage { get; set; } = new ObservableCollection<PackVoltage>();
        public ObservableCollection<PackTemperature> PackTemperature { get; set; } = new ObservableCollection<PackTemperature>();

        public ObservableCollection<DataBasic> EEPROM { get; set; } // 不需要配置記憶體，序列化
        public ObservableCollection<EEPROM_FLAG> EEPROM_FLAGs { get; set; } // 不需要配置記憶體，序列化

        public BatteryData(int PackNum)
        {
            for(int i = 0; i < PackNum; i++)
            {
                PackVoltage.Add(new PackVoltage(i + 1));
                PackTemperature.Add(new PackTemperature(i + 1));
            }
        }
    }
}
