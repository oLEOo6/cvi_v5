﻿using CVI_Lib;
using CVI_v5.Common;
using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CVI_v5.Model
{
    public class ConfigData : GalaSoft.MvvmLight.ViewModelBase
    {
        public int ModbusID { get; set; } = 1;
        public string ComPort { get; set; } = "COM1";
        public int Baud { get; set; } = 9600;
        public int CANspeed { get; set; } = 250;
        public int PackNum { get; set; } = 1;
        public int VoltageCellNum { get; set; } = 12;
        public int TemperatureCellNum { get; set; } = 12;

        public string LogFileName { get; set; } = "Unset";
        public string LogPath { get; set; } = @"C:\";

        public int VoltageOutlier { get; set; } = 30;
        public int VoltageDynamicOutlier { get; set; } = 60;
        public double VoltageHigh { get; set; } = 4.2;
        public double VoltageLow { get; set; } = 2.8;
        public int TemperatureOutlier { get; set; } = 20;
        public int TemperatureHigh { get; set; } = 80;
        public int TemperatureLow { get; set; } = -1;
        public int LogInterval { get; set; } = 3;
        public string Password { get; set; }

        public int CommunicationInterface { get; set; } = 0;

        private ConfigMethod ConfigMethod;

        public ConfigData(ConfigMethod ConfigMethod)
        {
            this.ConfigMethod = ConfigMethod;

            this.ConfigMethod.ReadAllSettings();

            Initilize();
        }

        public void Initilize()
        {
            ModbusID = ReadInt("ModbusID");
            ComPort = ReadString("ComPort");
            Baud = ReadInt("Baud");
            CANspeed = ReadInt("CANspeed");
            PackNum = ReadInt("PackNum");
            VoltageCellNum = ReadInt("VoltageCellNum");
            TemperatureCellNum = ReadInt("TemperatureCellNum");

            LogFileName = ReadString("LogFileName");
            LogPath = ReadString("LogPath");

            VoltageOutlier = ReadInt("VoltageOutlier");
            VoltageDynamicOutlier = ReadInt("VoltageDynamicOutlier");
            VoltageHigh = ReadDouble("VoltageHigh");
            VoltageLow = ReadDouble("VoltageLow");
            TemperatureOutlier = ReadInt("TemperatureOutlier");
            TemperatureHigh = ReadInt("TemperatureHigh");
            TemperatureLow = ReadInt("TemperatureLow");
            LogInterval = ReadInt("LogInterval");
            Password = ReadString("Password");

            CommunicationInterface = ReadInt("CommunicationInterface");
        }
        public void SaveAll()
        {
            Save("ModbusID", ModbusID);
            Save("ComPort", ComPort);
            Save("Baud", Baud);
            Save("CANspeed", CANspeed);
            Save("PackNum", PackNum);
            Save("VoltageCellNum", VoltageCellNum);
            Save("TemperatureCellNum", TemperatureCellNum);

            Save("LogFileName", LogFileName);
            Save("LogPath", LogPath);

            Save("VoltageOutlier", VoltageOutlier);
            Save("VoltageDynamicOutlier", VoltageDynamicOutlier);
            Save("VoltageHigh", VoltageHigh);
            Save("VoltageLow", VoltageLow);
            Save("TemperatureOutlier", TemperatureOutlier);
            Save("TemperatureHigh", TemperatureHigh);
            Save("TemperatureLow", TemperatureLow);
            Save("LogInterval", LogInterval);
            Save("Password", Password);

            Save("CommunicationInterface", CommunicationInterface);

            System.Diagnostics.Trace.WriteLine("Config Saved!");
        }

        private string ReadString(string key)
        {
            return ConfigMethod.ReadSetting(key);
        }
        private int ReadInt(string key)
        {
            return Convert.ToInt32(ConfigMethod.ReadSetting(key));
        }
        private double ReadDouble(string key)
        {
            return Convert.ToDouble(ConfigMethod.ReadSetting(key));
        }

        private void Save(string key, string value)
        {
            ConfigMethod.AddUpdateAppSettings(key, value);
        }
        private void Save(string key, int value)
        {
            ConfigMethod.AddUpdateAppSettings(key, value.ToString());
        }
        private void Save(string key, double value)
        {
            ConfigMethod.AddUpdateAppSettings(key, value.ToString());
        }
    }
}
