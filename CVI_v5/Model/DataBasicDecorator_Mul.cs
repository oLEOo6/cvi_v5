﻿using System;

namespace CVI_v5.Model
{
    [Serializable]
    class DataBasicDecorator_Mul : DataBasicDecorator
    {
        private DataBasic DataBasic;
        public override int Index
        {
            get
            {
                return DataBasic.Index;
            }
            set
            {
                DataBasic.Index = value;
            }
        }
        public override int Length
        {
            get
            {
                return DataBasic.Length;
            }
            set
            {
                DataBasic.Length = value;
            }
        } //改了會有問題
        public override bool BigEndian
        {
            get
            {
                return DataBasic.BigEndian;
            }
            set
            {
                DataBasic.BigEndian = value;
            }
        }
        public override bool Sign
        {
            get
            {
                return DataBasic.Sign;
            }
            set
            {
                DataBasic.Sign = value;
            }
        }
        public override byte[] Data
        {
            get
            {
                return DataBasic.Data;
            }
            set
            {
                DataBasic.Data = value;
                OnPropertyChanged("Value");
            }
        }
        public override string Name
        {
            get
            {
                return DataBasic.Name;
            }
            set
            {
                DataBasic.Name = value;
            }
        }
        public override string Unit
        {
            get
            {
                return DataBasic.Unit;
            }
            set
            {
                DataBasic.Unit = value;
            }
        }
        public override EEtype EEtype { get => DataBasic.EEtype; set => DataBasic.EEtype = value; }

        public double ValueMul { get; set; }

        public DataBasicDecorator_Mul(DataBasic dataBasic, double value)
        {
            this.DataBasic = dataBasic;
            this.ValueMul = value;
        }

        public override double GetValue()
        {
            return DataBasic.GetValue() * ValueMul;
        }

        public override void SetValue(double Input)
        {
            DataBasic.SetValue(Input / ValueMul);
        }
    }
}
