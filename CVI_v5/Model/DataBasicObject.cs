﻿using System;
using System.Diagnostics;

namespace CVI_v5.Model
{
    [Serializable]
    public class DataBasicObject : DataBasic
    {
        public DataBasicObject(int Index, int Length, bool BigEndian, bool Sign, string Name, string Unit, EEtype eEtype)
        {
            this.Index = Index;
            this.Length = Length;
            this.BigEndian = BigEndian;
            this.Sign = Sign;
            this.Name = Name;
            this.Unit = Unit;
            this.EEtype = eEtype;

            this.Data = new byte[Length];
        }
        public DataBasicObject(int Length, bool BigEndian, bool Sign, string Name, string Unit, EEtype eEtype) : this(0, Length, BigEndian, Sign, Name, Unit, eEtype)
        {
            
        }
        public DataBasicObject() { }

        public override double GetValue()
        {
            double temp = 0;

            if (Sign)
            {
                if (!BigEndian)
                {
                    for (int i = 0; i < Length; i++)
                        temp += Data[i] * Math.Pow(256, i);

                    if ((Data[Length - 1] & 0x80) == 0x80)
                        temp -= Math.Pow(2, Length * 8);
                }
                else
                {
                    for (int i = 0; i < Length; i++)
                        temp += Data[i] * Math.Pow(256, Length - 1 - i);

                    if ((Data[0] & 0x80) == 0x80)
                        temp -= Math.Pow(2, Length * 8);
                }
            }
            else
            {
                if (!BigEndian)
                {
                    for (int i = 0; i < Length; i++)
                        temp += Data[i] * Math.Pow(256, i);
                }
                else
                {
                    for (int i = 0; i < Length; i++)
                        temp += Data[i] * Math.Pow(256, Length -1 - i);
                }
            }

            return temp;
        }

        // 只支援到 4bytes，因為int
        public override void SetValue(double Input)
        {
            byte[] temp = new byte[Length];
            var INT = (int)Input;

            if (!Sign)
                Debug.Assert(Input >= 0);

            var ary = BitConverter.GetBytes(INT);
            Array.Copy(ary, temp, Length);

            if (BigEndian)
                Array.Reverse(temp);

            Data = temp;
        }
    }
}
