﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CVI_v5.Model
{
    public class ParallelData
    {
        public DataName ErrorAddress { get; set; } = new DataName("ErrorAddress");
        public DataName Status { get; set; } = new DataName("Status");
        public DataName SOC { get; set; } = new DataName("SOC");
        public DataName Voltage { get; set; } = new DataName("Voltage");
        public DataName Current { get; set; } = new DataName("Current");
        public DataName CurrentCHG { get; set; } = new DataName("CurrentCHG");
        public DataName CurrentDSG { get; set; } = new DataName("CurrentDSG");
        public DataName TemperatureMax { get; set; } = new DataName("TemperatureMax");
        public DataName TemperatureMin { get; set; } = new DataName("TemperatureMin");
        public DataName VoltageMax { get; set; } = new DataName("VoltageMax");
        public DataName VoltageMin { get; set; } = new DataName("VoltageMin");
    }
}
