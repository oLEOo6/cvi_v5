﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CVI_v5.Model
{
    public enum EEtype
    {
        Voltage,
        Temperature,
        Current,
        Information,
        Capacity,
        History,
        Calibration
    }

    public class EEtypes
    {
        
    }
}
