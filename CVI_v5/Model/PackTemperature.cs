﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CVI_v5.Model
{
    public class PackTemperature
    {
        public int Index { get; set; }
        public ObservableCollection<DataName> Cell { get; set; } = new ObservableCollection<DataName>();

        public DataName Average { get; set; } = new DataName("Average");
        public DataName Max { get; set; } = new DataName("Max");
        public DataName Min { get; set; } = new DataName("Min");
        public DataName Range { get; set; } = new DataName("Range");
        public bool RangeFlag { get; set; } = false;

        public PackTemperature(int PackNumber) : this(PackNumber, 12) { }
        public PackTemperature(int PackNumber, int CellNumber)
        {
            this.Index = PackNumber;
            for (int i = 0; i < CellNumber; i++)
                Cell.Add(new DataName(Index + "-TemperatureCell_" + (i + 1)));
        }
    }
}
