﻿using CVI_Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CVI_v5.Model
{
    [Serializable]
    public class EEPROM_FLAG : ViewModelBase
    {
        public string B7 { get; set; } = "7";
        public string B6 { get; set; } = "6";
        public string B5 { get; set; } = "5";
        public string B4 { get; set; } = "4";
        public string B3 { get; set; } = "3";
        public string B2 { get; set; } = "2";
        public string B1 { get; set; } = "1";
        public string B0 { get; set; } = "0";

        public string Name { get; set; }
        public int Index { get; set; }
        public byte BByte { get; set; }
    }
}
