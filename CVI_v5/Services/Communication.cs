﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CVI_v5.Services
{
    public abstract class Communication
    {
        public bool ConnectFlag { get; set; } = false;
        public int LoopDelay { get; set; }

        //public abstract bool Initilize();
        //public abstract void Reconnect();
        //public abstract void ReadCycle();
        //public abstract byte[] Read();
        //public abstract void Write(byte[] dataArray);
        //public abstract void WriteEE();
        //public abstract void ReadEE();
    }
}
