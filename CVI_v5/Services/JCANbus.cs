﻿using CVI_Lib.Laike;
using CVI_v5.Common;
using CVI_v5.ViewModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CVI_v5.Services
{
    class JCANbus : Communication, ICommunication
    {
        private UInt32 m_DeviceHandle = 0;
        private UInt32 m_dwChannel = 0;
        private readonly int CANspeed;
        private int SendDelay = 10; // 2021/04/26 Testing ::24 is OK

        private readonly MainViewModel MainVM;
        public JCANbus(MainViewModel MainVM, int CANspeed)
        {
            this.MainVM = MainVM;
            this.CANspeed = CANspeed;

            LoopDelay = 100;
        }

        public bool Initilize()
        {
            Close();

            char arg = '0';
            m_DeviceHandle = CanCmd.CAN_DeviceOpen(CanCmd.USBCAN_1CH, 0, ref arg);
            if (m_DeviceHandle == 0)
                return false;

            CAN_InitConfig config = new CAN_InitConfig
            {
                nFilter = 0,
                bMode = 0,
                dwAccCode = 0x00000000,
                dwAccMask = 0xFFFFFFFF,
                nBtrType = 1   // 位定时参数模式(1表示SJA1000,0表示LPC21XX)
            };
            //0014-1M  0016-800K  001C-500K  011C-250K  031C-125K  041C-100K  091C-50K  181C-20K  311C-10K  BFFF-5K
            if (CANspeed == 250)
            {
                config.dwBtr0 = 0x01;
                config.dwBtr1 = 0x1C;
            }
            else if (CANspeed == 1000)
            {
                config.dwBtr0 = 0x00;
                config.dwBtr1 = 0x14;
            }
            else // Default 500K
            {
                config.dwBtr0 = 0x00;
                config.dwBtr1 = 0x1C;
            }

            if (CanCmd.CAN_RESULT_OK != CanCmd.CAN_ChannelStart(m_DeviceHandle, m_dwChannel, ref config))
                return false;
            return true;
        }
        public void Reconnect()
        {
            Task.Run(() =>
            {
                while (!ConnectFlag)
                {
                    ConnectFlag = Initilize();
                    ErrorOutput.ErrorLog("CANBUS initilize fail!");
                    System.Threading.Thread.Sleep(2000);
                }
            });
        }

        unsafe public List<CAN_DataFrame> Read()
        {
            var data = new List<CAN_DataFrame>();
            //if (CanCmd.CAN_GetReceiveCount(m_DeviceHandle, m_dwChannel) == 0) return null;

            UInt32 con_maxlen = 50; // buffer size
            IntPtr pt = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(CAN_DataFrame)) * (Int32)con_maxlen);
            var ReceiveDataCount = CanCmd.CAN_ChannelReceive(m_DeviceHandle, m_dwChannel, pt, con_maxlen, 100);

            //var data = new List<CAN_DataFrame>();
            if (ReceiveDataCount != 0)
            {
                for (UInt32 i = 0; i < ReceiveDataCount; i++)
                {
                    CAN_DataFrame obj = (CAN_DataFrame)Marshal.PtrToStructure((IntPtr)((UInt32)pt + i * Marshal.SizeOf(typeof(CAN_DataFrame))), typeof(CAN_DataFrame));
                    data.Add(obj);
                }
            }
            else //error
            {
                CAN_ErrorInformation err = new CAN_ErrorInformation();  //error message
                if (CanCmd.CAN_GetErrorInfo(m_DeviceHandle, m_dwChannel, ref err) == CanCmd.CAN_RESULT_OK)
                    Trace.WriteLine("Communication Error: " + err.uErrorCode);
                else
                    Trace.WriteLine("No data");
                //return null;
            }

            Marshal.FreeHGlobal(pt);//RAM Release

            return data;
        }

        public void Write(byte[] dataArray)
        {
            throw new NotImplementedException();
        }
        unsafe public Boolean USB_CAN_Send(UInt32 ID, byte[] DATA, byte length)
        {
            CAN_DataFrame sendobj = new CAN_DataFrame();
            sendobj.nSendType = 1; //normal send
            sendobj.bRemoteFlag = 0; //data frame   1:remote frame
            sendobj.bExternFlag = 1; // 1:Extended
            sendobj.uID = ID;
            sendobj.nDataLen = length; // 8

            for (byte i = 0; i < length; i++)
                sendobj.arryData[i] = DATA[i];

            if (CanCmd.CAN_ChannelSend(m_DeviceHandle, m_dwChannel, ref sendobj, 1) == 0)
            {
                Console.WriteLine("Laike CAN_USB send Fail!");
                return false;
            }
            return true;
        }

        public void Close()
        {
            CanCmd.CAN_ChannelStop(m_DeviceHandle, m_dwChannel);
            CanCmd.CAN_DeviceClose(m_DeviceHandle);
        }

        #region 通訊協議
        unsafe public void ReadCycle()
        {
            if (!ConnectFlag)
            {
                ErrorOutput.ErrorLog("CANBUS not CONNECT!");
                Thread.Sleep(2000);
                return;
            }
            var DataGet = Read();
            if (DataGet.Count == 0) return;

            var VoltageUpdateFlag = false;
            var TemperaureUpdateFlag = false;

            foreach (var data in DataGet)
            {
                var Page  = (data.uID & 0x000F0000) >> 16; // EE Page
                var Block = (data.uID & 0x00F00000) >> 20; // EE Block
                var PlusE = (data.uID & 0x01000000) >> 24; // CAN DP
                var index = (PlusE << 3) + (Block << 8) + (Page << 4);
                //var index = (data.uID >> 16) & 0x01ff; // Include CAN DP
                var ID = data.uID & 0xfe00ffff; // 0x1800F0EE or 0x1900F0EE is OK

                var FunctionCode = data.arryData[0];
                #region MainData
                if (ID == 0x1800A7F4)
                {
                    if (FunctionCode == 0x01)
                    {
                        MainVM.Battery.Voltage.Value = (UInt16)(data.arryData[1] + data.arryData[2] * 256) / 100.0;
                        MainVM.Battery.Temperature.Value = (Int16)(data.arryData[3] + data.arryData[4] * 256) / 100.0;
                        MainVM.Battery.Current.Value = (Int16)(data.arryData[5] + data.arryData[6] * 256) / 100.0;
                        MainVM.Battery.SOC.Value = data.arryData[7];
                    }
                    else if (FunctionCode == 0x02)
                    {
                        MainVM.Battery.Status.Value = (UInt16)(data.arryData[1] + data.arryData[2] * 256);
                        MainVM.Battery.ErrorCode.Value = data.arryData[3];
                        MainVM.Battery.Relay.Value = data.arryData[4];
                    }
                    else if (FunctionCode == 0x03)
                    {
                        //int tt = int.Parse(data.arryData[4], NumberStyles.HexNumber) + (int.Parse(data.arryData[5], NumberStyles.HexNumber) << 8);
                        //Battery.BMSVersion = (UInt16)(data.arryData[1] + data.arryData[2]);
                        //Battery.FIRMWAREVersion = (UInt16)(data.arryData[3] + data.arryData[4]);

                        //((tt >> 11) & 0b11111) * 100000 + ((tt >> 7) & 0b1111) * 1000 + ((tt >> 2) & 0b11111) * 10 + (tt & 0b11);
                        //電流版暫時不用
                        //tt = int.Parse(data.arryData[6], NumberStyles.HexNumber) + (int.Parse(data.arryData[7], NumberStyles.HexNumber) << 8);
                        //EEPROMViewModel.FirmwareVersion_CUR.Value = ((tt >> 11) & 0b11111) * 100000 + ((tt >> 7) & 0b1111) * 1000 + ((tt >> 2) & 0b11111) * 10 + (tt & 0b11);
                    }

                }
                #endregion
                #region Cell Voltage&Temperature
                else if (ID == 0x1C00A7F4)
                {
                    var PackNum = (FunctionCode >> 4) - 1; // -1 0開始
                    var func = FunctionCode & 0x0f;

                    //if (PackNum < 0 || PackNum >= PACK) break;//防錯誤

                    #region Cell Voltage
                    if (func >= 0 && func <= 3)//Voltage
                    {
                        for (int i = 0; i < 3; i++)
                            MainVM.Battery.PackVoltage[PackNum].Cell[func * 3 + i].Value = (data.arryData[1 + i * 2] + data.arryData[2 + i * 2] * 256) / 10000.0;

                        VoltageUpdateFlag = true;
                    }
                    #endregion
                    #region Cell Temperature
                    else if (func >= 8 && func <= 11)//Temperature
                    {
                        for (int i = 0; i < 3; i++)
                           MainVM.Battery.PackTemperature[PackNum].Cell[(func - 8) * 3 + i].Value = (Int16)(data.arryData[1 + i * 2] + data.arryData[2 + i * 2] * 256) / 100.0;

                        TemperaureUpdateFlag = true;
                    }
                    #endregion
                }
                #endregion
                #region EE
                else if (ID == 0x1800F0EE)
                {
                    Trace.WriteLine("EE: " + index);
                    // 效能差 待改善!!!!!!!!!!!
                    foreach(var ee in MainVM.Battery.EEPROM)
                    {
                        if (ee.Index >= index && ee.Index < index + 8)
                        {
                            var test = new byte[ee.Length];
                            for (int i = 0; i < ee.Length; i++)
                                test[i] = data.arryData[ee.Index - index + i];
                            ee.Data = test;
                        }
                    }
                    foreach (var ee in MainVM.Battery.EEPROM_FLAGs)
                    {
                        if (ee.Index >= index && ee.Index < index + 8)
                            ee.BByte = data.arryData[ee.Index - index];
                    }
                }
                #endregion
            }

            if (VoltageUpdateFlag)
            {
                for (int i = 0; i < MainVM.Config.PackNum; i++)
                {
                    // Cell Voltage Max, Min, Avg, Dif, Flag
                    double tV = 0, tVmax = double.MinValue, tVmin = double.MaxValue;
                    for (int j = 0; j < MainVM.Config.VoltageCellNum; j++)
                    {
                        tV += MainVM.Battery.PackVoltage[i].Cell[j].Value;
                        tVmax = tVmax > MainVM.Battery.PackVoltage[i].Cell[j].Value ? tVmax : MainVM.Battery.PackVoltage[i].Cell[j].Value;
                        tVmin = tVmin < MainVM.Battery.PackVoltage[i].Cell[j].Value ? tVmin : MainVM.Battery.PackVoltage[i].Cell[j].Value;
                    }
                    MainVM.Battery.PackVoltage[i].Total.Value = tV;   //cell總電壓
                    MainVM.Battery.PackVoltage[i].Max.Value = tVmax;  //cell最大電壓
                    MainVM.Battery.PackVoltage[i].Min.Value = tVmin;  //cell最小電壓
                    MainVM.Battery.PackVoltage[i].Range.Value = (tVmax - tVmin) * 1000;  //cell電壓差 :Unit=>mV

                    double Vref = (MainVM.Battery.Current.Value < 0.2 && MainVM.Battery.Current.Value > -0.2) ? MainVM.Config.VoltageOutlier : MainVM.Config.VoltageDynamicOutlier; //Flag_Value
                    MainVM.Battery.PackVoltage[i].RangeFlag = MainVM.Battery.PackVoltage[i].Range.Value > Vref; //Range_Flag
                }
                #region PackVoltage 最大最小差值運算
                double ttVmax = double.MinValue, ttVmin = double.MaxValue;
                for (int i = 0; i < MainVM.Config.PackNum; i++)
                {
                    ttVmax = ttVmax > MainVM.Battery.PackVoltage[i].Total.Value ? ttVmax : MainVM.Battery.PackVoltage[i].Total.Value;
                    ttVmin = ttVmin < MainVM.Battery.PackVoltage[i].Total.Value ? ttVmin : MainVM.Battery.PackVoltage[i].Total.Value;
                }
                MainVM.Battery.PackVoltageMax.Value = ttVmax;
                MainVM.Battery.PackVoltageMin.Value = ttVmin;
                double VVref = (MainVM.Battery.Current.Value < 0.2 && MainVM.Battery.Current.Value > -0.2) ? MainVM.Config.VoltageOutlier : MainVM.Config.VoltageDynamicOutlier; //Flag_Value
                MainVM.Battery.PackVoltageRangeFlag = ttVmax - ttVmin >= VVref;
                #endregion
            }
            if (TemperaureUpdateFlag)
            {
                for (int i = 0; i < MainVM.Config.PackNum; i++)
                {
                    // Cell Voltage Max, Min, Avg, Dif, Flag
                    double tT = 0, tTmax = double.MinValue, tTmin = double.MaxValue;
                    for (int j = 0; j < MainVM.Config.TemperatureCellNum; j++)
                    {
                        tT += MainVM.Battery.PackTemperature[i].Cell[j].Value;
                        tTmax = tTmax > MainVM.Battery.PackTemperature[i].Cell[j].Value ? tTmax : MainVM.Battery.PackTemperature[i].Cell[j].Value;
                        tTmin = tTmin < MainVM.Battery.PackTemperature[i].Cell[j].Value ? tTmin : MainVM.Battery.PackTemperature[i].Cell[j].Value;
                    }
                    MainVM.Battery.PackTemperature[i].Average.Value = tT / MainVM.Config.TemperatureCellNum; //cell平均溫度
                    MainVM.Battery.PackTemperature[i].Max.Value = tTmax; //cell最高溫度
                    MainVM.Battery.PackTemperature[i].Min.Value = tTmin; //cell最低溫度
                    MainVM.Battery.PackTemperature[i].Range.Value = tTmax - tTmin; //cell溫度差

                    MainVM.Battery.PackTemperature[i].RangeFlag = MainVM.Battery.PackTemperature[i].Range.Value > MainVM.Config.TemperatureOutlier; //Range_Flag
                }

                #region PackTemperature 最大最小差值運算
                double ttTmax = double.MinValue, ttTmin = double.MaxValue;
                for (int i = 0; i < MainVM.Config.PackNum; i++)
                {
                    ttTmax = ttTmax > MainVM.Battery.PackTemperature[i].Max.Value ? ttTmax : MainVM.Battery.PackTemperature[i].Max.Value;
                    ttTmin = ttTmin < MainVM.Battery.PackTemperature[i].Min.Value ? ttTmin : MainVM.Battery.PackTemperature[i].Min.Value;
                }
                MainVM.Battery.PackTemperatureMax.Value = ttTmax;
                MainVM.Battery.PackTemperatureMin.Value = ttTmin;
                MainVM.Battery.PackTemperatureRange.Value = ttTmax - ttTmin;
                MainVM.Battery.PackTemperatureRangeFlag = ttTmax - ttTmin >= MainVM.Config.TemperatureOutlier;
                #endregion
            }

            Thread.Sleep(LoopDelay);
        }

        public void WriteEE()
        {
            foreach (var ee in MainVM.Battery.EEPROM)
            {
                var dataSend = new byte[8];
                dataSend[0] = (byte)'W';
                dataSend[1] = (byte)(ee.Index >> 8);
                dataSend[2] = (byte)(ee.Index);
                dataSend[3] = (byte)(ee.Length);
                for (int i = 0; i < ee.Length; i++)
                    dataSend[4 + i] = ee.Data[i];

                USB_CAN_Send(0x1800EEF0, dataSend, 8);

                Thread.Sleep(SendDelay); // 2021/04/26
            }
            foreach (var ee in MainVM.Battery.EEPROM_FLAGs)
            {
                var dataSend = new byte[8];
                dataSend[0] = 0x58;
                dataSend[1] = (byte)(ee.Index >> 8);
                dataSend[2] = (byte)(ee.Index);
                dataSend[3] = 0xff;
                dataSend[4] = ee.BByte;
                USB_CAN_Send(0x1800EEF0, dataSend, 8);

                Thread.Sleep(SendDelay); // 2021/04/26
            }

            // Load
            USB_CAN_Send(0x1800EEF0, new byte[] { 0xFB, 0, 0, 0, 0, 0, 0, 0 }, 8);
        }

        public void ReadEE()
        {
            USB_CAN_Send(0x1800EEF0, new byte[] { (byte)'R', 0, 0, 0, 0, 0, 0, 0xAE }, 8);
        }

        public void DefaultProtection()
        {
            USB_CAN_Send(0x1800EEF0, new byte[] { 0xFA, 0, 0, 0, 0, 0, 0, 0 }, 8);
        }

        public void LoadSetting()
        {
            USB_CAN_Send(0x1800EEF0, new byte[] { 0xFB, 0, 0, 0, 0, 0, 0, 0 }, 8);
        }

        public void ClearCounter()
        {
            USB_CAN_Send(0x1800EEF0, new byte[] { 0xFD, 0, 0, 0, 0, 0, 0, 0 }, 8);
        }

        public void ClearHistory()
        {
            USB_CAN_Send(0x1800EEF0, new byte[] { 0xFE, 0, 0, 0, 0, 0, 0, 0 }, 8);
        }

        public void ClearAll()
        {
            USB_CAN_Send(0x1800EEF0, new byte[] { 0xFF, 0, 0, 0, 0, 0, 0, 0 }, 8);
        }

        public bool Connected()
        {
            return ConnectFlag;
        }

        public void VSOC()
        {
            USB_CAN_Send(0x1800F4F0, new byte[] { 0xF0, 0, 0, 0, 0, 0, 0, 0x0F }, 8);
        }

        public void CurrentCalibration(byte[] Current)
        {
            if (Current.Length < 2)
                return;
            byte sum = (byte)~(0xF1 + Current[0] + Current[1]);
            USB_CAN_Send(0x1800F4F0, new byte[] { 0xF1, Current[0], Current[1], 0, 0, 0, 0, sum}, 8);
        }
        #endregion
    }
}
