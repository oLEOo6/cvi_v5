﻿using CVI_v5.Common;
using CVI_v5.ViewModel;
using System;
using System.IO.Ports;
using System.Threading;

namespace CVI_v5.Services
{
    class JSerialPort : Communication , ICommunication
    {
        private SerialPort aSerialPort;
        private string Port = "COM1";
        private int Baud;

        private object LockObj = new object();

        private readonly MainViewModel MainVM;
        public JSerialPort(MainViewModel MainVM, int baud)
        {
            this.MainVM = MainVM;
            this.Baud = baud;
            this.Port = MainVM.Config.ComPort;

            LoopDelay = 2800;
        }

        public bool Initilize()
        {
            aSerialPort = new SerialPort()
            {
                PortName = Port,
                BaudRate = Baud,
                Parity = Parity.None,
                StopBits = StopBits.One,
                DataBits = 8,
                RtsEnable = true,
                DtrEnable = true,
                //WriteTimeout = 100,
                //ReadTimeout = 100,
            };
            //aSerialPort.DataReceived += ASerialPort_DataReceived;

            try
            {
                aSerialPort.Open();
            }
            catch (Exception)
            {
                ErrorOutput.ErrorLog(Port + "open fail!");
                return false;
            }
            
            return true;
        }
        public void Reconnect()
        {
            System.Threading.Tasks.Task.Run(() =>
            {
                while (true)
                {
                    if(!ConnectFlag)
                        ConnectFlag = Initilize();
                    if (ConnectFlag)
                    {
                        Thread.Sleep(10000);
                        continue;
                    }
                    else
                    {
                        foreach(var s in SerialPort.GetPortNames())
                        {
                            Port = s;
                            ConnectFlag = Initilize();
                            if (ConnectFlag)
                                break;
                            else
                                ErrorOutput.ErrorLog(s +"\tConnect Fail!");

                            Thread.Sleep(500);
                        }
                    }
                }
            });
        }

        public byte[] Read()
        {
            var buffer = new byte[aSerialPort.BytesToRead];
            aSerialPort.Read(buffer, 0, buffer.Length);
            return buffer;
        }

        public void Write(byte[] dataArray)
        {
            aSerialPort.Write(dataArray, 0, dataArray.Length);
        }

        public byte[] ModbusHandShake(byte[] dataSend)
        {
            try
            {
                if (!ConnectFlag)
                    throw new Exception("Not Cennect");

                lock (LockObj)
                {
                    Write(dataSend);
                    Thread.Sleep(50); // Modbus Delay Time
                    return Read();
                }
            }
            catch(Exception e)
            {
                ConnectFlag = false;
                ErrorOutput.ErrorLog("ComBus HandShake fail! \t" + e.Message);
                return new byte[] { 0 };
            }
            
        }

        public byte[] ModbusSendDataConvert(byte ID, byte FunctionCode, UInt16 StartAddress, UInt16 RegisterNumber)
        {
            var result = new byte[6];
            result[0] = ID;
            result[1] = FunctionCode;
            result[2] = (byte)(StartAddress >> 8);
            result[3] = (byte)StartAddress;
            result[4] = (byte)(RegisterNumber >> 8);
            result[5] = (byte)RegisterNumber;
            ModBus.AppendCRC(result);

            return result;
        }
        public byte[] ModbusSendDataConvert(byte ID, byte FunctionCode, UInt16 StartAddress, UInt16 RegisterNumber, byte[] Data)
        {
            var result = new byte[6 + Data.Length];
            result[0] = ID;
            result[1] = FunctionCode;
            result[2] = (byte)(StartAddress >> 8);
            result[3] = (byte)StartAddress;
            result[4] = (byte)(RegisterNumber >> 8);
            result[5] = (byte)RegisterNumber;
            Array.Copy(Data, 0, result, 6, Data.Length);
            ModBus.AppendCRC(result);

            return result;
        }

        #region 通訊協議
        public void ReadCycle()
        {
            #region Cell Voltage
            var PrepareToSend = ModbusSendDataConvert((byte)MainVM.Config.ModbusID, 0x04, 0x0110, (UInt16)(MainVM.Config.PackNum * 0x10));
            var DataGet = ModbusHandShake(PrepareToSend);
            var result = ModBus.AckConfirm(DataGet, 3 + (MainVM.Config.PackNum * 0x10 * 2) + 2, (byte)MainVM.Config.ModbusID, 0x04); // 長度: ID(1) + FunctionCode(1) + Count(1) + 16 * 2 * PACK + CRC(2)

            if (result)
            {
                // Cell Voltage 填入
                for (int i = 0; i < MainVM.Config.PackNum; i++)
                {
                    for (int j = 0; j < MainVM.Config.VoltageCellNum; j++)
                        MainVM.Battery.PackVoltage[i].Cell[j].Value = (DataGet[5 + i * 16 * 2 + j * 2] * 256 + DataGet[6 + i * 16 + j * 2]) / 10000.0;

                    // Cell Voltage Max, Min, Avg, Dif, Flag
                    double tV = 0, tVmax = double.MinValue, tVmin = double.MaxValue;
                    for (int j = 0; j < MainVM.Config.VoltageCellNum; j++)
                    {
                        tV += MainVM.Battery.PackVoltage[i].Cell[j].Value;
                        tVmax = tVmax > MainVM.Battery.PackVoltage[i].Cell[j].Value ? tVmax : MainVM.Battery.PackVoltage[i].Cell[j].Value;
                        tVmin = tVmin < MainVM.Battery.PackVoltage[i].Cell[j].Value ? tVmin : MainVM.Battery.PackVoltage[i].Cell[j].Value;
                    }
                    MainVM.Battery.PackVoltage[i].Total.Value = tV;   //cell總電壓
                    MainVM.Battery.PackVoltage[i].Max.Value = tVmax;  //cell最大電壓
                    MainVM.Battery.PackVoltage[i].Min.Value = tVmin;  //cell最小電壓
                    MainVM.Battery.PackVoltage[i].Range.Value = (tVmax - tVmin) * 1000;  //cell電壓差  :Unit=>mV

                    double Vref = (MainVM.Battery.Current.Value < 0.2 && MainVM.Battery.Current.Value > -0.2) ? MainVM.Config.VoltageOutlier : MainVM.Config.VoltageDynamicOutlier; //Flag_Value
                    MainVM.Battery.PackVoltage[i].RangeFlag = MainVM.Battery.PackVoltage[i].Range.Value > Vref; //Range_Flag
                }
            }
            else
            {
                ErrorOutput.ErrorLog("Cell電壓讀取失敗");
            }
            #endregion
            #region Cell Temperature
            PrepareToSend = ModbusSendDataConvert((byte)MainVM.Config.ModbusID, 0x04, 0x0210, (UInt16)(MainVM.Config.PackNum * 0x10));
            DataGet = ModbusHandShake(PrepareToSend);
            result = ModBus.AckConfirm(DataGet, 3 + (MainVM.Config.PackNum * 0x10 * 2) + 2, (byte)MainVM.Config.ModbusID, 0x04); // 長度: ID(1) + FunctionCode(1) + Count(1) + 16 * 2 * PACK + CRC(2)

            if (result)
            {
                for (int i = 0; i < MainVM.Config.PackNum; i++)
                {
                    for (int j = 0; j < MainVM.Config.TemperatureCellNum; j++)
                        MainVM.Battery.PackTemperature[i].Cell[j].Value = (Int16)(DataGet[5 + i * 16 * 2 + j * 2] * 256 + DataGet[6 + i * 16 + j * 2]) / 100.0;

                    // Cell Voltage Max, Min, Avg, Dif, Flag
                    double tT = 0, tTmax = double.MinValue, tTmin = double.MaxValue;
                    for (int j = 0; j < MainVM.Config.TemperatureCellNum; j++)
                    {
                        tT += MainVM.Battery.PackTemperature[i].Cell[j].Value;
                        tTmax = tTmax > MainVM.Battery.PackTemperature[i].Cell[j].Value ? tTmax : MainVM.Battery.PackTemperature[i].Cell[j].Value;
                        tTmin = tTmin < MainVM.Battery.PackTemperature[i].Cell[j].Value ? tTmin : MainVM.Battery.PackTemperature[i].Cell[j].Value;
                    }
                    MainVM.Battery.PackTemperature[i].Average.Value = tT / MainVM.Config.TemperatureCellNum; //cell平均溫度
                    MainVM.Battery.PackTemperature[i].Max.Value = tTmax; //cell最高溫度
                    MainVM.Battery.PackTemperature[i].Min.Value = tTmin; //cell最低溫度
                    MainVM.Battery.PackTemperature[i].Range.Value = tTmax - tTmin; //cell溫度差

                    MainVM.Battery.PackVoltage[i].RangeFlag = MainVM.Battery.PackTemperature[i].Range.Value > MainVM.Config.TemperatureOutlier; //Range_Flag
                }
            }
            else
            {
                ErrorOutput.ErrorLog("Cell溫度讀取失敗");
            }
            #endregion
            #region PackVoltage 最大最小差值運算
            double ttVmax = double.MinValue, ttVmin = double.MaxValue;
            for (int i = 0; i < MainVM.Config.PackNum; i++)
            {
                ttVmax = ttVmax > MainVM.Battery.PackVoltage[i].Total.Value ? ttVmax : MainVM.Battery.PackVoltage[i].Total.Value;
                ttVmin = ttVmin < MainVM.Battery.PackVoltage[i].Total.Value ? ttVmin : MainVM.Battery.PackVoltage[i].Total.Value;
            }
            MainVM.Battery.PackVoltageMax.Value = ttVmax;
            MainVM.Battery.PackVoltageMin.Value = ttVmin;
            double VVref = (MainVM.Battery.Current.Value < 0.2 && MainVM.Battery.Current.Value > -0.2) ? MainVM.Config.VoltageOutlier : MainVM.Config.VoltageDynamicOutlier; //Flag_Value
            MainVM.Battery.PackVoltageRangeFlag = ttVmax - ttVmin <= VVref;
            #endregion
            #region PackTemperature 最大最小差值運算
            double ttTmax = double.MinValue, ttTmin = double.MaxValue;
            for (int i = 0; i < MainVM.Config.PackNum; i++)
            {
                ttTmax = ttTmax > MainVM.Battery.PackTemperature[i].Max.Value ? ttTmax : MainVM.Battery.PackTemperature[i].Max.Value;
                ttTmin = ttTmin < MainVM.Battery.PackTemperature[i].Min.Value ? ttTmin : MainVM.Battery.PackTemperature[i].Min.Value;
            }
            MainVM.Battery.PackTemperatureMax.Value = ttTmax;
            MainVM.Battery.PackTemperatureMin.Value = ttTmin;
            MainVM.Battery.PackTemperatureRange.Value = ttTmax - ttTmin;
            MainVM.Battery.PackTemperatureRangeFlag = ttTmax - ttTmin >= MainVM.Config.TemperatureOutlier;
            #endregion

            #region MainData
            PrepareToSend = ModbusSendDataConvert((byte)MainVM.Config.ModbusID, 0x04, 0x0001, 0x0012);
            DataGet = ModbusHandShake(PrepareToSend);
            result = ModBus.AckConfirm(DataGet, 3 + (0x0012 * 2) + 2, (byte)MainVM.Config.ModbusID, 0x04); // 長度: ID(1) + FunctionCode(1) + Count(1) + 0x12 * 2 + CRC(2)

            if (result)
            {
                MainVM.Battery.Voltage.Value = (DataGet[3] * 256 + DataGet[4]) / 100.0;
                MainVM.Battery.Current.Value = (Int16)(DataGet[5] * 256 + DataGet[6]) / 100.0;
                MainVM.Battery.SOC.Value = (UInt16)(DataGet[7] * 256 + DataGet[8]);
                MainVM.Battery.TemperatureMax.Value = (Int16)(DataGet[9] * 256 + DataGet[10]) / 100.0;
                MainVM.Battery.TemperatureMin.Value = (Int16)(DataGet[11] * 256 + DataGet[12]) / 100.0;
                MainVM.Battery.VoltageMax.Value = ((DataGet[13] * 256) + DataGet[14]) / 10000.0;
                MainVM.Battery.VoltageMin.Value = ((DataGet[15] * 256) + DataGet[16]) / 10000.0;
                MainVM.Battery.CurrentCHG.Value = (Int16)((DataGet[17] * 256) + DataGet[18]) / 100.0;
                MainVM.Battery.CurrentDSG.Value = (Int16)((DataGet[19] * 256) + DataGet[20]) / 100.0;
                MainVM.Battery.Status.Value = (UInt16)(DataGet[21] * 256 + DataGet[22]);
                MainVM.Battery.ErrorCode.Value = (UInt16)(DataGet[23] * 256 + DataGet[24]);
                MainVM.Battery.Relay.Value = (UInt16)(DataGet[25] * 256 + DataGet[26]);
                MainVM.Battery.Cycle.Value = (UInt16)(DataGet[27] * 256 + DataGet[28]);
                MainVM.Battery.ChargeTime.Value = (UInt16)(DataGet[29] * 256 + DataGet[30]);
                MainVM.Battery.UseTime.Value = (UInt16)(DataGet[31] * 256 + DataGet[32]);
                MainVM.Battery.Year.Value = (UInt16)(DataGet[33] * 256 + DataGet[34]);
                MainVM.Battery.Week.Value = (UInt16)(DataGet[35] * 256 + DataGet[36]);
                MainVM.Battery.FirmwareVersion.Value = (UInt16)(DataGet[37] * 256 + DataGet[38]);
            }
            #endregion

            #region 並聯資料
            PrepareToSend = ModbusSendDataConvert((byte)MainVM.Config.ModbusID, 0x25, 0x0001, 0x000B);
            DataGet = ModbusHandShake(PrepareToSend);
            result = ModBus.AckConfirm(DataGet, 3 + (0x000B * 2) + 2, (byte)MainVM.Config.ModbusID, 0x25); // 長度: ID(1) + FunctionCode(1) + Count(1) + 16 * 2 * PACK + CRC(2)

            if (result)
            {
                MainVM.Battery.ParallelData.ErrorAddress.Value = (UInt16)(DataGet[3] * 256 + DataGet[4]);
                MainVM.Battery.ParallelData.Status.Value = (UInt16)(DataGet[5] * 256 + DataGet[6]);
                MainVM.Battery.ParallelData.SOC.Value = (UInt16)(DataGet[7] * 256 + DataGet[8]);
                MainVM.Battery.ParallelData.Voltage.Value = (UInt16)(DataGet[9] * 256 + DataGet[10]) / 100.0;
                MainVM.Battery.ParallelData.Current.Value = (Int16)(DataGet[11] * 256 + DataGet[12]) / 100.0;
                MainVM.Battery.ParallelData.CurrentCHG.Value = (Int16)((DataGet[13] * 256) + DataGet[14]) / 100.0;
                MainVM.Battery.ParallelData.CurrentDSG.Value = (Int16)((DataGet[15] * 256) + DataGet[16]) / 100.0;
                MainVM.Battery.ParallelData.TemperatureMax.Value = (Int16)((DataGet[17] * 256) + DataGet[18]) / 100.0;
                MainVM.Battery.ParallelData.TemperatureMin.Value = (Int16)((DataGet[19] * 256) + DataGet[20]) / 100.0;
                MainVM.Battery.ParallelData.VoltageMax.Value = (UInt16)(DataGet[21] * 256 + DataGet[22]) / 10000.0;
                MainVM.Battery.ParallelData.VoltageMin.Value = (UInt16)(DataGet[23] * 256 + DataGet[24]) / 10000.0;
            }
            #endregion

            Thread.Sleep(LoopDelay);
        }

        public void WriteEE()
        {
            throw new NotImplementedException();
        }

        public void ReadEE()
        {
            throw new NotImplementedException();
        }

        public void DefaultProtection()
        {
            throw new NotImplementedException();
        }

        public void LoadSetting()
        {
            throw new NotImplementedException();
        }

        public void ClearCounter()
        {
            throw new NotImplementedException();
        }

        public void ClearHistory()
        {
            throw new NotImplementedException();
        }

        public void ClearAll()
        {
            throw new NotImplementedException();
        }

        public bool Connected()
        {
            throw new NotImplementedException();
        }

        public void VSOC()
        {
            throw new NotImplementedException();
        }

        public void CurrentCalibration(byte[] Current)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
