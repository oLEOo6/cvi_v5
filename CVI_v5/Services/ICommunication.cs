﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CVI_v5.Services
{
    public interface ICommunication
    {
        void Reconnect();
        void WriteEE();
        void ReadEE();
        bool Initilize();
        void Write(byte[] dataArray);

        void VSOC();
        void CurrentCalibration(byte[] Current);

        void ReadCycle();
        void DefaultProtection();
        void LoadSetting();
        void ClearCounter();
        void ClearHistory();
        void ClearAll();

        bool Connected();
    }
}
