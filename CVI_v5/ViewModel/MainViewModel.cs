using CVI_Lib;
using CVI_v5.Common;
using CVI_v5.Model;
using CVI_v5.Services;
using GalaSoft.MvvmLight;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Input;
using System.Xml.Serialization;

namespace CVI_v5.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : GalaSoft.MvvmLight.ViewModelBase
    {
        public ConfigData Config { get; set; }
        public BatteryData Battery { get; set; }
        public ICommunication CommInterface { get; set; }
        public LogMethod Log { get; set; } = new LogMethod();

        public GalaSoft.MvvmLight.ViewModelBase Page { get; set; }

        private void Initilize()
        {
            Config = new ConfigData(new ConfigMethod());
            Battery = new BatteryData(Config.PackNum);
            
            XmlDeSerialize();

            switch (Config.CommunicationInterface)
            {
                case (int)CommunicationInterface.CANBUS:
                    CommInterface = new JCANbus(this, Config.CANspeed);
                    break;
                case (int)CommunicationInterface.MODBUS:
                    CommInterface = new JSerialPort(this, Config.Baud);
                    break;
            }
            CommInterface.Reconnect();
            System.Threading.Tasks.Task.Run(() =>
            {
                System.Threading.Thread.Sleep(1000); // delay
                while (true)
                    CommInterface.ReadCycle();
            });

            // Log Object Add
            Log_Object_Add();

            //test
            System.Threading.Tasks.Task.Run(() =>
            {
                while(Page == null)
                {
                    System.Threading.Thread.Sleep(50);
                    Page = CommonServiceLocator.ServiceLocator.Current.GetInstance<DetailViewModel>();
                }
            });
        }
        public ICommand ClosingCommand => new RelayCommand(() =>
        {
            XmlSerialize(); // EEPROM_Collection Save
            Config.SaveAll();
        });

        private void Log_Object_Add()
        {
            Log.DataSet.Add(Battery.SOC);
            Log.DataSet.Add(Battery.Voltage);
            Log.DataSet.Add(Battery.Current);
            Log.DataSet.Add(Battery.Temperature);
            Log.DataSet.Add(Battery.Status);
            Log.DataSet.Add(Battery.Relay);

            for(int i = 0; i < Config.PackNum; i++)
            {
                for (int j = 0; j < Config.VoltageCellNum; j++)
                    Log.DataSet.Add(Battery.PackVoltage[i].Cell[j]);
                Log.DataSet.Add(Battery.PackVoltage[i].Total);
                Log.DataSet.Add(Battery.PackVoltage[i].Max);
                Log.DataSet.Add(Battery.PackVoltage[i].Min);
                Log.DataSet.Add(Battery.PackVoltage[i].Range);

                for (int j = 0; j < Config.TemperatureCellNum; j++)
                    Log.DataSet.Add(Battery.PackTemperature[i].Cell[j]);
                Log.DataSet.Add(Battery.PackTemperature[i].Average);
                Log.DataSet.Add(Battery.PackTemperature[i].Max);
                Log.DataSet.Add(Battery.PackTemperature[i].Min);
                Log.DataSet.Add(Battery.PackTemperature[i].Range);
            }

            Log.DataSet.Add(Battery.PackVoltageMax);
            Log.DataSet.Add(Battery.PackVoltageMin);
            Log.DataSet.Add(Battery.PackVoltageRange);
            Log.DataSet.Add(Battery.PackTemperatureMax);
            Log.DataSet.Add(Battery.PackTemperatureMin);
            Log.DataSet.Add(Battery.PackTemperatureRange);
        }

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel()
        {
            #region blend
            //if (IsInDesignMode)
            //{
            //    // Code runs in Blend --> create design time data.
            //}
            //else
            //{
            //    // Code runs "for real"
            //}
            #endregion

            //Services.ICommunication i = new Services.JSerialPort();
            //i.Initilize();

            Initilize();
        }

        #region ICommand
        public ICommand DetialViewCommand => new RelayCommand(() =>
        {
            Page = CommonServiceLocator.ServiceLocator.Current.GetInstance<DetailViewModel>();
        });
        public ICommand EEPROMViewCommand => new RelayCommand(() =>
        {
            Page = CommonServiceLocator.ServiceLocator.Current.GetInstance<EEPROMViewModel>();
        });
        public ICommand SettingViewCommand => new RelayCommand(() =>
        {
            Page = CommonServiceLocator.ServiceLocator.Current.GetInstance<SettingViewModel>();
        });
        public ICommand OthersViewCommand => new RelayCommand(() =>
        {
            Page = CommonServiceLocator.ServiceLocator.Current.GetInstance<OthersViewModel>();
        });

        public ICommand LogCommand => new RelayCommand(() =>
        {
            try
            {
                if (!Log.Running)
                {
                    Config.LogPath = Log.PathSelect(Config.LogPath);
                    Log.FirstTime(Config.LogFileName, Config.LogPath, Config.LogInterval * 1000);
                }
                else
                    Log.LogStop();
            }
            catch (Exception) { Log.Running = false; }
            
        }, CommInterface.Connected);
        #endregion

        private void XmlSerialize()
        {
            //XmlSerializer writer = new XmlSerializer(typeof(ObservableCollection<DataBasic>));
            //StreamWriter file = new StreamWriter(@"xml.xml");
            //writer.Serialize(file, Battery.EEPROM);
            //file.Close();

            using (Stream fStream = new FileStream("EE.bin", FileMode.OpenOrCreate, FileAccess.Write, FileShare.None))
            {
                new BinaryFormatter().Serialize(fStream, Battery.EEPROM);
            }
            using (Stream fStream = new FileStream("EEFLAG.bin", FileMode.OpenOrCreate, FileAccess.Write, FileShare.None))
            {
                new BinaryFormatter().Serialize(fStream, Battery.EEPROM_FLAGs);
            }
        }
        private void XmlDeSerialize()
        {
            //XmlSerializer reader = new XmlSerializer(typeof(ObservableCollection<DataBasic>));
            //StreamReader file = new StreamReader(@"xml.xml");
            //Battery.EEPROM = reader.Deserialize(file) as ObservableCollection<DataBasic>;
            //file.Close();

            try
            {
                using (Stream fStream = File.OpenRead("EE.bin"))
                {
                    ObservableCollection<DataBasic> pis = new BinaryFormatter().Deserialize(fStream) as ObservableCollection<DataBasic>;
                    Battery.EEPROM = pis;
                }
            }
            catch (Exception)
            {
                ErrorOutput.ErrorLog("�䤣�� EE.bin!");
                Battery.EEPROM = new ObservableCollection<DataBasic>();
            }

            try
            {
                using (Stream fStream = File.OpenRead("EEFLAG.bin"))
                {
                    ObservableCollection<EEPROM_FLAG> pis = new BinaryFormatter().Deserialize(fStream) as ObservableCollection<EEPROM_FLAG>;
                    Battery.EEPROM_FLAGs = pis;
                }
            }
            catch (Exception)
            {
                ErrorOutput.ErrorLog("�䤣�� EEFLAG.bin!");
                Battery.EEPROM_FLAGs = new ObservableCollection<EEPROM_FLAG>();
            }
        }
    }
}