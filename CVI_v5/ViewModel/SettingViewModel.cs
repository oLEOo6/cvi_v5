﻿using CVI_v5.Model;
using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CVI_v5.ViewModel
{
    public class SettingViewModel : ViewModelBase
    {
        public MainViewModel MainViewModel { get; set; } = CommonServiceLocator.ServiceLocator.Current.GetInstance<MainViewModel>();
        public ConfigData ConfigData { get; set; }

        public SettingViewModel()
        {
            ConfigData = MainViewModel.Config;
        }
    }
}
