﻿using CVI_Lib;
using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace CVI_v5.ViewModel
{
    public class OthersViewModel : GalaSoft.MvvmLight.ViewModelBase
    {
        public MainViewModel MainViewModel { get; set; } = CommonServiceLocator.ServiceLocator.Current.GetInstance<MainViewModel>();

        public double CurrentCalibrationValue { get; set; }

        #region Command
        // EEPROM
        public ICommand DefaultProtectionCommand => new RelayCommand(() =>
        {
            MainViewModel.CommInterface.DefaultProtection();
        });
        public ICommand LoadSettingCommand => new RelayCommand(() =>
        {
            MainViewModel.CommInterface.LoadSetting();
        });
        public ICommand ClearCounterCommand => new RelayCommand(() =>
        {
            MainViewModel.CommInterface.ClearCounter();
        });
        public ICommand ClearHistoryCommand => new RelayCommand(() =>
        {
            MainViewModel.CommInterface.ClearHistory();
        });
        public ICommand ClearAllCommand => new RelayCommand(() =>
        {
            MainViewModel.CommInterface.ClearAll();
        });
        public ICommand VSOCCommand => new RelayCommand(() =>
        {
            MainViewModel.CommInterface.VSOC();
        });
        public ICommand CurrentCalibrationCommand => new RelayCommand(() =>
        {
            var val = (Int16)(CurrentCalibrationValue * 100);
            MainViewModel.CommInterface.CurrentCalibration(BitConverter.GetBytes(val));
        });
        #endregion
    }
}
