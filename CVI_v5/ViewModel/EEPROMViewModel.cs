﻿using CVI_Lib;
using CVI_v5.Model;
using CVI_v5.View;
using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using ViewModelBase = GalaSoft.MvvmLight.ViewModelBase;

namespace CVI_v5.ViewModel
{
    public class EEPROMViewModel : ViewModelBase
    {
        public MainViewModel MainViewModel { get; set; } = CommonServiceLocator.ServiceLocator.Current.GetInstance<MainViewModel>();

        // EEPROM新增用
        public string Name { get; set; }
        public string Unit { get; set; }
        public int Index { get; set; } = 0;
        public int Length { get; set; } = 1;
        public bool BigEndian { get; set; }
        public bool Sign { get; set; }
        public EEtype Eetype { get; set; }
        public DataBasic DataBasicObject { get; set; } // For 點選

        // EEPROM Flag
        public string FlagName { get; set; }
        public int FlagAddress { get; set; }
        public EEPROM_FLAG EEPROM_FLAG_Selected { get; set; } // For 點選

        public Calculate Calculate { get; set; }
        public double Val { get; set; } = 0;

        #region Command
        // EEPROM
        public ICommand AddCommand => new RelayCommand(() =>
        {
            int count = 0;
            foreach (var ee in MainViewModel.Battery.EEPROM)
            {
                if (ee.EEtype == this.Eetype)
                {
                    MainViewModel.Battery.EEPROM.Insert(count, new DataBasicObject(this.Index, this.Length, this.BigEndian, this.Sign, this.Name, this.Unit, this.Eetype)
                    {
                        EEtype = this.Eetype
                    });
                    return;
                }
                count++;
            }
            MainViewModel.Battery.EEPROM.Add(new DataBasicObject(this.Index, this.Length, this.BigEndian, this.Sign, this.Name, this.Unit, this.Eetype)
            {
                EEtype = this.Eetype
            });

            //MainViewModel.Battery.EEPROM = new System.Collections.ObjectModel.ObservableCollection<DataBasic>(MainViewModel.Battery.EEPROM.OrderBy(a => { return a.EEtype; })); // Sort
        });
        public ICommand AdjustCommand => new RelayCommand(() =>
        {
            if (DataBasicObject == null)
            {
                MessageBox.Show("Not Select");
                return;
            }

            var temp = DataBasicObject;
            MainViewModel.Battery.EEPROM.Remove(DataBasicObject);

            switch (Calculate)
            {
                case Calculate.Add:
                    temp = new DataBasicDecorator_Add(temp, Val);
                    break;
                case Calculate.Mul:
                    temp = new DataBasicDecorator_Mul(temp, Val);
                    break;
            }
            MainViewModel.Battery.EEPROM.Add(temp);
        }); 
        public ICommand RemoveCommand => new RelayCommand(() =>
        {
            if (DataBasicObject == null)
            {
                MessageBox.Show("Not Select");
                return;
            }

            MainViewModel.Battery.EEPROM.Remove(DataBasicObject);
        });

        // EEPROM Flag
        public ICommand FlagAddCommand => new RelayCommand(() =>
        {
            MainViewModel.Battery.EEPROM_FLAGs.Add(new EEPROM_FLAG()
            {
                Name = FlagName,
                Index = FlagAddress
            });
        });
        public ICommand FlagRemoveCommand => new RelayCommand(() =>
        {
            if (EEPROM_FLAG_Selected == null)
            {
                MessageBox.Show("Not Select");
                return;
            }

            MainViewModel.Battery.EEPROM_FLAGs.Remove(EEPROM_FLAG_Selected);
        });

        // Not Finish
        public ICommand ReadCommand => new RelayCommand(() =>
        {
            MainViewModel.CommInterface.ReadEE();
        });
        public ICommand WriteCommand => new RelayCommand(() =>
        {
            MainViewModel.CommInterface.WriteEE();
        });
        #endregion
    }
}
